#pragma once
#include <vector>
#include "Grid.h"

struct color {
	float R; //red
	float G; //green
	float B; //blue
};

struct agent {	
	unsigned int x;
	unsigned int y;
	int velocity;
	color color;
};

enum direction {
	up = 0,
	right = 1,
	down = 2,
	left = 3
};

class AgentsGrid : public Grid
{
private:
	std::vector <agent> agents; //regular agents -> blue agents
	std::vector <agent> perpendicularAgents; //perpendicular -> red agents
	void denyAccessRectangle(unsigned int x, unsigned int y, unsigned int width, unsigned int height);
	void setAgent(int x, int y, int velocity, float red, float green, float blue,
		bool perpendicularAgent);
	bool simulationOver;
	bool isAgentsRoadClear(int x, int y, int velocity, direction agentDirection);
	bool moveAgent(std::vector <agent> &agents, unsigned int agentIndex, direction agentDirection);
	void moveAgentRandomly(std::vector <agent> &agents, unsigned int agentIndex, direction Direction1, direction Direction2, direction Direction3);
	bool deflectFromWalls;
	int randomInteger(int min, int max);

	//regular agents -> blue agents
	std::vector <unsigned int> startFlowRate;
	std::vector <unsigned int> endFlowRate;
	//perpendicular -> red agents
	std::vector <unsigned int> startPFlowRate;
	std::vector <unsigned int> endPFlowRate;

	//regularAgents
	unsigned int getCurrentStartFlowRate();
	unsigned int getCurrentEndFlowRate();
	//perpendicularAgents
	unsigned int getCurrentPStartFlowRate();
	unsigned int getCurrentPEndFlowRate();

	float getAverageFlowRate(std::vector <unsigned int> flowRate);

	bool reachedEnd;
	bool intersection;

public:
	AgentsGrid();
	AgentsGrid(unsigned int gridSizeX, unsigned int gridSizeY, bool deflection, bool isIntersection);
	~AgentsGrid();
	void setAgentsRectangle(int x, int y, int width, int height, int velocity, float red, float green, float blue,
		bool perpendicularAgent);
	void setAgentsRandomly(int x, int y, int width, int height, int velocity, float red, float green, float blue, float agentsDensity, 
		bool perpendicularAgent);
	bool checkStartArea(int x, int y, int width, int height);
	void singleIteration();
	bool isSimulationOver();
	
	//regularAgents
	color getAgentColor(int agentIndex);
	int getAgentX(int agentIndex);
	int getAgentY(int agentIndex);
	unsigned int getAgentsNumber();

	//perpendicularAgents
	color getPAgentColor(int agentIndex);
	int getPAgentX(int agentIndex);
	int getPAgentY(int agentIndex);
	unsigned int getPAgentsNumber();

	void createNarrowing();
	void createRandomNarrowing();
	void createParameterizedNarrowing(unsigned int a);
	void createDoor(unsigned int a);
	
	float getAverageFlowRateStart();
	float getAverageFlowRateEnd();
	
	void saveFlowRatesToFile(float agentsDensity, float perpendicularAgentsDensity, std::string filename, bool intersection);
};

