#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <thread>
#include <chrono>
#include <iostream>
#include <array>
#include "AgentsGrid.h"
#include "graphical_functions.h"

const float inaccessible_red = 0.4f;
const float inaccessible_green = 0.4f;
const float inaccessible_blue = 0.4f;
const unsigned int spawn_rate = 30;

void drawPixel(float redColor, float greenColor, float blueColor, float x, float y, float size)
{
	glColor3f(redColor, greenColor, blueColor);

	glBegin(GL_QUADS);
	glVertex2f(x, y);
	glVertex2f(x + size, y);
	glVertex2f(x + size, y - size);
	glVertex2f(x, y - size);
	glEnd();
}

void Display(int width, int height, AgentsGrid &agentsGrid)
{
	float ratio = width / (float)height;
	//ustawiwamy lewy, dolny r�g okna (0,0) i przekazujemy szeroko�� i wysoko�� okna w pikselach
	glViewport(0, 0, width, height);

	glClearColor(inaccessible_red, inaccessible_green, 0.4, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(-ratio, ratio, -1.f, 1.f, 1.f, -1.f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	int xSize = agentsGrid.getSizeX();
	int ySize = agentsGrid.getSizeY();
	float dSize;

	//red - not allowed to be there, blue - occupied, green - free
	
	if (xSize >= ySize) {
		dSize = 2.0 / (float)xSize;
		float offset = (float)ySize / 2.0f * dSize;
		
		for (int i = 0; i < xSize; i++) {
			for (int j = 0; j < ySize; j++) {
				
				if (agentsGrid.getCellAccessibility(i, j) == false) {
					drawPixel(inaccessible_red, inaccessible_green, inaccessible_blue, (float)i*dSize-1.0f, (float)j*dSize - offset, dSize);
				} else if (agentsGrid.getCellOccupation(i,j) == false) {
					//drawPixel(0.0, 0.0, 1.0, (float)i*dSize-1.0f, (float)j*dSize - offset, dSize);
				} else if (agentsGrid.getCellOccupation(i, j) == true) {
					drawPixel(0.0, 1.0, 0.0, (float)i*dSize-1.0f, (float)j*dSize - offset, dSize);
				} else {
					drawPixel(1.0, 0.0, 0.0, (float)i*dSize - 1.0f, (float)j*dSize - offset, dSize);
				}
				
			}
		}

		color agentColor;
		int x, y;
		for (int i = 0; i < agentsGrid.getAgentsNumber(); i++) {
			agentColor = agentsGrid.getAgentColor(i);
			x = agentsGrid.getAgentX(i);
			y = agentsGrid.getAgentY(i);

			drawPixel(agentColor.R, agentColor.G, agentColor.B, (float)x*dSize - 1.0f, (float)y*dSize - offset, dSize);
		}

		for (int i = 0; i < agentsGrid.getPAgentsNumber(); i++) {
			agentColor = agentsGrid.getPAgentColor(i);
			x = agentsGrid.getPAgentX(i);
			y = agentsGrid.getPAgentY(i);

			drawPixel(agentColor.R, agentColor.G, agentColor.B, (float)x*dSize - 1.0f, (float)y*dSize - offset, dSize);
		}
	} else {
		dSize = 2.0 / ySize;
	}	

	glFlush();
}


static void error_callback(int error, const char* description) {
	fputs(description, stderr);
}

//handling escape key
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

/*
startArea[0] - startX
startArea[1] - startY
startArea[2] - sizeX
startArea[3] - sizeY
*/
void Visualization(AgentsGrid &agentsGrid, bool slowDown, float agentDensity, float perpendicularAgentDensity,
	std::array <unsigned int, 4> startArea1) {
	GLFWwindow* window;

	glfwSetErrorCallback(error_callback);

	if (!glfwInit())
		exit(EXIT_FAILURE);

	window = glfwCreateWindow(900, 600, "Praca magisterska", NULL, NULL);

	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
	glfwSetKeyCallback(window, key_callback);

	double time = glfwGetTime();

	std::array <unsigned int, 4> startArea2 = { 0,0,0,0 };
	if (perpendicularAgentDensity > 0.0f) {
		startArea2[0] = startArea1[1];
		startArea2[1] = startArea1[0];
		startArea2[2] = startArea1[3];
		startArea2[3] = startArea1[2];
	}

	int i = 0;

	while (!glfwWindowShouldClose(window)) {
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);

		Display(width, height, agentsGrid);
		
		if (agentsGrid.isSimulationOver() == false) {

			if (agentsGrid.checkStartArea(startArea1[0], startArea1[1], startArea1[2], startArea1[3])) {
				agentsGrid.setAgentsRandomly(startArea1[0], startArea1[1], startArea1[2], startArea1[3], 1, 0.0f, 0.0f, 1.0f, agentDensity, false);
			}

			if (perpendicularAgentDensity > 0.0f) {
				if (agentsGrid.checkStartArea(startArea2[0], startArea2[1], startArea2[2], startArea2[3])) {
					agentsGrid.setAgentsRandomly(startArea2[0], startArea2[1], startArea2[2], startArea2[3], 1, 1.0f, 0.0f, 0.0f, perpendicularAgentDensity, true);
				}
			}

			agentsGrid.singleIteration();
			
			/*
			std::cout << "AS: " << agentsGrid.getCurrentStartFlowRate() << ", AE: " << agentsGrid.getCurrentEndFlowRate();
			std::cout << " PAS: " << agentsGrid.getCurrentPStartFlowRate() << ", PAE: " << agentsGrid.getCurrentPEndFlowRate() << std::endl;
			std::cout << "Iteration number: " << i << std::endl;
			*/

			i++;
			if (slowDown) {
				std::this_thread::sleep_for(std::chrono::milliseconds(500));
			}
		}

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	//agentsGrid.saveFlowRatesToFile(whiteDensity, blackDensity, "data.txt");

	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}
