#pragma once

struct cell {
	bool is_free;
	bool is_available;
};

class Grid
{
protected:
	unsigned int size_x;
	unsigned int size_y;
	cell** cell_grid;	

public:
	Grid();
	Grid::Grid(unsigned int sizeX, unsigned int sizeY);
	~Grid();

	void writeGrid();
	void denyCellAccess(int x, int y);
	unsigned int getSizeX();
	unsigned int getSizeY();
	bool getCellAccessibility(int x, int y);
	void setCellAccessibility(int x, int y, bool isAccessible);
	bool getCellOccupation(int x, int y);
	void setCellOccupation(int x, int y, bool isFree);
};