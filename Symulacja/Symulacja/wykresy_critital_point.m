data1 = load('critical_points_DOOR.txt');
data2 = load('critical_points_GRADUAL.txt');

a = data1(:,1); b = data2(:,1);
crit_dens1 = data1(:,2); crit_dens2 = data2(:,2);
crit_agents1 = data1(:,4); crit_agents2 = data2(:,4);
crit_dens_stdev1 = data1(:,3); crit_dens_stdev2 = data2(:,3);
crit_agents_stdev1 = data1(:,5); crit_agents_stdev2 = data2(:,5);

figure(1)
h1 = errorbar(a, crit_dens1,crit_dens_stdev1)
set(h1,"marker","o"); set(h1,"markeredgecolor","red")
set(h1,"markerfacecolor","red"); set(h1,"linestyle","none")
title('Krytyczna gestosc agentow (A) - drzwi'); xlabel('A'); ylabel('Krytyczna gestosc agentow')
ylim([0 0.61]);

figure(2)
h2 = errorbar(a, crit_agents1,crit_agents_stdev1)
set(h2,"marker","o"); set(h2,"markeredgecolor","red")
set(h2,"markerfacecolor","red"); set(h2,"linestyle","none")
title('Krytyczna liczba agentow na wejsciu (A) - drzwi'); xlabel('A'); ylabel('Krytyczna liczba agentow na wejsciu')

figure(3)
h3 = errorbar(b, crit_dens2,crit_dens_stdev2)
set(h3,"marker","o"); set(h3,"markeredgecolor","red")
set(h3,"markerfacecolor","red"); set(h3,"linestyle","none")
title('Krytyczna gestosc agentow (B) - zwezenie stopniowe'); xlabel('B'); ylabel('Krytyczna gestosc agentow')

figure(4)
h4 = errorbar(b, crit_agents2,crit_agents_stdev2)
set(h4,"marker","o"); set(h4,"markeredgecolor","red")
set(h4,"markerfacecolor","red"); set(h4,"linestyle","none")
title('Krytyczna liczba agentow na wejsciu (B) - zwezenie stopniowe'); xlabel('B'); ylabel('Krytyczna liczba agentow na wejsciu')