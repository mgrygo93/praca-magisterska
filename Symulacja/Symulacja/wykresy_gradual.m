data0 = load('data_a=0.txt');
data1 = load('data_a=1.txt');
data2 = load('data_a=2.txt');
data3 = load('data_a=3.txt');
data4 = load('data_a=4.txt');
data5 = load('data_a=5.txt');
data6 = load('data_a=6.txt');

density0 = data0(:,1);
start_point0 = data0(:,3);
end_point0 = data0(:,4);

density1 = data1(:,1);
start_point1 = data1(:,3);
end_point1 = data1(:,4);

density2 = data2(:,1);
start_point2 = data2(:,3);
end_point2 = data2(:,4);

density3 = data3(:,1);
start_point3 = data3(:,3);
end_point3 = data3(:,4);

density4 = data4(:,1);
start_point4 = data4(:,3);
end_point4 = data4(:,4);

density5 = data5(:,1);
start_point5 = data5(:,3);
end_point5 = data5(:,4);

density6 = data6(:,1);
start_point6 = data6(:,3);
end_point6 = data6(:,4);

#sr. agenci (gestosc)
figure(1)
plot(density0, end_point0,
  density1, end_point1,
  density2, end_point2,
  density3, end_point3,
  density4, end_point4,
  density5, end_point5,
  density6, end_point6);
set (0, "defaultlinelinewidth", 1.5) 
title('Wyjscie (wejscie) - zwezenie stopniowe'); legend("B=0", "B=1", "B=2", "B=3", "B=4", "B=5", "B=6", "Location", "NorthWest");
xlabel('Gestosc agentow'); ylabel('Sr. liczba agentow (wyjscie)');
xlim([0 0.61]); ylim([0 13]);

#sr. agenci (sr. agenci)
figure(2)
plot(start_point0, end_point0,
  start_point1, end_point1,
  start_point2, end_point2,
  start_point3, end_point3,
  start_point4, end_point4,
  start_point5, end_point5,
  start_point6, end_point6);
set (0, "defaultlinelinewidth", 1.5) 
title('Wyjscie (wejscie) - zwezenie stopniowe'); legend("B=0", "B=1", "B=2", "B=3", "B=4", "B=5", "B=6", "Location", "NorthWest");
xlabel('Sr. liczba agentow (wejscie)'); ylabel('Sr. liczba agentow (wyjscie)');
xlim([0 13]); ylim([0 13]);