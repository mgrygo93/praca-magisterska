#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>
#include "AgentsGrid.h"

AgentsGrid::AgentsGrid() {
}

AgentsGrid::AgentsGrid(unsigned int gridSizeX, unsigned int gridSizeY, bool deflection, bool isIntersection) {
	size_x = gridSizeX;
	size_y = gridSizeY;
	deflectFromWalls = deflection;
	simulationOver = false;
	reachedEnd = false;
	intersection = isIntersection;

	if (!intersection) {
		cell_grid = new cell*[size_y];
		for (unsigned int i = 0; i < size_y; i++) {
			cell_grid[i] = new cell[size_x];
		}

		for (unsigned int i = 0; i < size_y; i++) {
			for (unsigned int j = 0; j < size_x; j++) {
				cell_grid[i][j].is_available = true;
				cell_grid[i][j].is_free = true;
			}
		}
	} else {
		unsigned int tmp = size_y;
		size_y = size_x;

		cell_grid = new cell*[size_y];
		for (unsigned int i = 0; i < size_y; i++) {
			cell_grid[i] = new cell[size_x];
		}

		for (unsigned int i = 0; i < size_y; i++) {
			for (unsigned int j = 0; j < size_x; j++) {
				cell_grid[i][j].is_available = true;
				cell_grid[i][j].is_free = true;
			}
		}

		unsigned int width = (unsigned int)((size_x - tmp)/2);
		unsigned int height = width;
		
		denyAccessRectangle(0, 0, width, height);
		denyAccessRectangle(width+tmp, 0, width, height);
		denyAccessRectangle(0, width + tmp, width, height);
		denyAccessRectangle(width + tmp, width + tmp, width, height);
	}
}

AgentsGrid::~AgentsGrid() {
	for (unsigned int i = 0; i < size_y; i++) {
		delete[] cell_grid[i];
	}
	delete[] cell_grid;
}

void AgentsGrid::denyAccessRectangle(unsigned int x, unsigned int y, unsigned int width, unsigned int height) {
	for (unsigned int i = x; i < width+x; i++) {
		for (unsigned int j = y; j < height+y; j++) {
			setCellAccessibility(i, j, false);
		}		
	}	
}

void AgentsGrid::setAgent(int x, int y, int velocity, float red, float green, float blue,
	bool perpendicularAgent) {
	if (getCellAccessibility(x, y) == true) {
		setCellOccupation(x, y, false);
	}

	agent new_agent;
	new_agent.velocity = velocity;
	new_agent.x = x;
	new_agent.y = y;
	new_agent.color.R = red;
	new_agent.color.G = green;
	new_agent.color.B = blue;

	if (perpendicularAgent) {
		perpendicularAgents.push_back(new_agent);
	}
	else {
		agents.push_back(new_agent);
	}	
}

void AgentsGrid::setAgentsRectangle(int x, int y, int width, int height, int velocity, float red, float green, float blue,
	bool perpendicularAgent) {
	if (x + width > getSizeX() || y + height > getSizeY() || x < 0 || y < 0 || width < 0 || height < 0) {
		printf("Cell out of bounds! Invalid value given!\n");
	} else {
		for (unsigned int i = 0; i < width; i++) {
			for (unsigned int j = 0; j < height; j++) {
				setAgent(i + x, j + y, velocity, red, green, blue, perpendicularAgent);
			}
		}
	}
}

int randomInteger(int min, int max) {
	return min + (rand() % (int)(max - min + 1));
}

void AgentsGrid::setAgentsRandomly(int x, int y, int width, int height, int velocity, float red, float green, float blue, float agentsDensity, 
	bool perpendicularAgent) {
	if (x + width > getSizeX() || y + height > getSizeY() || x < 0 || y < 0 || width < 0 || height < 0) {
		printf("Cell out of bounds! Invalid value given!\n");
	} else if (agentsDensity > 1.0f || agentsDensity < 0.0f) {
		printf("Wrong agents' density value!\n");
	} else {
		double agent_x, agent_y;
		unsigned int createdAgents = 0;
		unsigned int availableCells = 0;
		
		for (unsigned int i = 0; i < width; i++) {
			for (unsigned int j = 0; j < height; j++) {
				if (getCellAccessibility(i + x, j + y) == true && getCellOccupation(i + x, j + y) == true) {
					availableCells++;
				}
			}
		}

		unsigned int agentsNumber = (unsigned int)(agentsDensity*width*height);

		if (availableCells < agentsNumber) {
			for (unsigned int i = 0; i < width; i++) {
				for (unsigned int j = 0; j < height; j++) {
					if (getCellAccessibility(i + x, j + y) == true && getCellOccupation(i + x, j + y) == true) {
						setAgent(i + x, j + y, velocity, red, green, blue, perpendicularAgent);
					}
				}
			}
		} else {
			while (createdAgents != agentsNumber) {
				//"-1" bo zwracamy sie do lokalizacji po indeksach czyli <min index, size-1>
				agent_x = randomInteger(x, x + width - 1);
				agent_y = randomInteger(y, y + height - 1);

				if (getCellAccessibility(agent_x, agent_y) == true && getCellOccupation(agent_x, agent_y) == true) {
					setAgent(agent_x, agent_y, velocity, red, green, blue, perpendicularAgent);
					createdAgents++;
				}
			}
		}
	}
}

bool AgentsGrid::checkStartArea(int x, int y, int width, int height) {
	unsigned int cellsNumber = width*height;
	unsigned int availableCells = 0;

	for (unsigned int i = 0; i < width; i++) {
		for (unsigned int j = 0; j < height; j++) {
			if (getCellAccessibility(i + x, j + y) == true && getCellOccupation(i + x, j + y) == true) {
				availableCells++;
			}
		}
	}

	if (cellsNumber == availableCells) {
		return true;
	} else {
		return false;
	}
}

bool AgentsGrid::isAgentsRoadClear(int x, int y, int velocity, direction agentDirection) {
	std::vector <bool> cellsAccessibility;
	bool cellState;
	bool roadClear = true;
	
	switch (agentDirection) {
	case up: {
		for (int i = 1; i <= velocity; i++) {
			cellState = getCellAccessibility(x, y + i) && getCellOccupation(x, y + i);
			cellsAccessibility.push_back(cellState);
		}
		break;
	}
	case right: {
		for (int i = 1; i <= velocity; i++) {
			cellState = getCellAccessibility(x + i, y) && getCellOccupation(x + i, y);
			cellsAccessibility.push_back(cellState);
		}
		break;
	}
	case down: {
		for (int i = 1; i <= velocity; i++) {
			cellState = getCellAccessibility(x, y - i) && getCellOccupation(x, y - i);
			cellsAccessibility.push_back(cellState);
		}
		break;
	}
	case left: {
		for (int i = 1; i <= velocity; i++) {
			cellState = getCellAccessibility(x - i, y) && getCellOccupation(x - i, y);
			cellsAccessibility.push_back(cellState);
		}
		break;
	}
	default:
		break;

	}

	if (std::find(cellsAccessibility.begin(), cellsAccessibility.end(), false) != cellsAccessibility.end()) {
		roadClear = false;
	}

	return roadClear;
}

bool AgentsGrid::moveAgent(std::vector <agent> &agents, unsigned int agentIndex, direction agentDirection) {
	switch (agentDirection) {
	case up: {
		if (isAgentsRoadClear(agents[agentIndex].x, agents[agentIndex].y, agents[agentIndex].velocity, up)) {
			setCellOccupation(agents[agentIndex].x, agents[agentIndex].y + agents[agentIndex].velocity, false);
			setCellOccupation(agents[agentIndex].x, agents[agentIndex].y, true);
			agents[agentIndex].y = agents[agentIndex].y + agents[agentIndex].velocity;

			return true;
		} else {
			return false;
		}
		break;
	}
	case right: {
		if (isAgentsRoadClear(agents[agentIndex].x, agents[agentIndex].y, agents[agentIndex].velocity, right)) {
			setCellOccupation(agents[agentIndex].x + agents[agentIndex].velocity, agents[agentIndex].y, false);
			setCellOccupation(agents[agentIndex].x, agents[agentIndex].y, true);
			agents[agentIndex].x = agents[agentIndex].x + agents[agentIndex].velocity;

			return true;
		} else {
			return false;
		}
		break;
	}
	case down: {
		if (isAgentsRoadClear(agents[agentIndex].x, agents[agentIndex].y, agents[agentIndex].velocity, down)) {
			setCellOccupation(agents[agentIndex].x, agents[agentIndex].y - agents[agentIndex].velocity, false);
			setCellOccupation(agents[agentIndex].x, agents[agentIndex].y, true);
			agents[agentIndex].y = agents[agentIndex].y - agents[agentIndex].velocity;

			return true;
		} else {
			return false;
		}
		break;
	}
	case left: {
		if (isAgentsRoadClear(agents[agentIndex].x, agents[agentIndex].y, agents[agentIndex].velocity, left)) {
			setCellOccupation(agents[agentIndex].x - agents[agentIndex].velocity, agents[agentIndex].y, false);
			setCellOccupation(agents[agentIndex].x, agents[agentIndex].y, true);
			agents[agentIndex].x = agents[agentIndex].x - agents[agentIndex].velocity;

			return true;
		}
		else {
			return false;
		}
		break;
	}
	default:
		return false;
		break;
	}	
}
	
int AgentsGrid::randomInteger(int min, int max) {
	return min + (rand() % (int)(max - min + 1));
}

void AgentsGrid::moveAgentRandomly(std::vector <agent> &agents, unsigned int agentIndex, direction Direction1, direction Direction2, direction Direction3) {
	bool hasMoved = false;

	int tmp = randomInteger(0, 2);

	switch (tmp) {
	case 0: {
		hasMoved = moveAgent(agents, agentIndex, Direction1);
		if (!hasMoved) {
			tmp = randomInteger(0, 1);
			if (tmp == 0) {
				hasMoved = moveAgent(agents, agentIndex, Direction2);
				if (!hasMoved) {
					hasMoved = moveAgent(agents, agentIndex, Direction3);
				}
			}
			else {
				hasMoved = moveAgent(agents, agentIndex, Direction3);
				if (!hasMoved) {
					hasMoved = moveAgent(agents, agentIndex, Direction2);
				}
			}
		}
		break;
	}
	case 1: {
		hasMoved = moveAgent(agents, agentIndex, Direction2);
		if (!hasMoved) {
			tmp = randomInteger(0, 1);
			if (tmp == 0) {
				hasMoved = moveAgent(agents, agentIndex, Direction1);
				if (!hasMoved) {
					hasMoved = moveAgent(agents, agentIndex, Direction3);
				}
			}
			else {
				hasMoved = moveAgent(agents, agentIndex, Direction3);
				if (!hasMoved) {
					hasMoved = moveAgent(agents, agentIndex, Direction1);
				}
			}
		}
		break;
	} 
	case 2: {
		hasMoved = moveAgent(agents, agentIndex, Direction3);
		if (!hasMoved) {
			tmp = randomInteger(0, 1);
			if (tmp == 0) {
				hasMoved = moveAgent(agents, agentIndex, Direction2);
				if (!hasMoved) {
					hasMoved = moveAgent(agents, agentIndex, Direction1);
				}
			}
			else {
				hasMoved = moveAgent(agents, agentIndex, Direction1);
				if (!hasMoved) {
					hasMoved = moveAgent(agents, agentIndex, Direction2);
				}
			}
		}
		break;
	} 
	default:
		break;
	}	
}

void AgentsGrid::singleIteration() {
	for (size_t i = 0; i < agents.size(); i++) {
		double p = (double)rand() / RAND_MAX;

		//moving agents
		if (agents[i].x + agents[i].velocity >= size_x) {
			setCellOccupation(agents[i].x, agents[i].y, true);
			agents.erase(agents.begin() + i);
			break;
		}

		bool hasMoved = false;
		float controlValue;
		if (deflectFromWalls) {
			controlValue = 0.1f;
		} else {
			controlValue = 0.0f;
		}

		if (p > controlValue) {//moving forward
			hasMoved = moveAgent(agents, i, right);
		} else {//wall deflection
			if (agents[i].y < getSizeY()/2) {
				hasMoved = moveAgent(agents, i, up);				
			} else {
				hasMoved = moveAgent(agents, i, down);
			}
		}
		
		if (!hasMoved) {
			moveAgentRandomly(agents, i, up, down, right);
		}		
	}	

	//perpendicular agents
	for (size_t i = 0; i < perpendicularAgents.size(); i++) {
		double p = (double)rand() / RAND_MAX;

		//moving agents
		if (perpendicularAgents[i].y + perpendicularAgents[i].velocity >= size_y) {
			setCellOccupation(perpendicularAgents[i].x, perpendicularAgents[i].y, true);
			perpendicularAgents.erase(perpendicularAgents.begin() + i);
			break;
		}

		bool hasMoved = false;
		float controlValue;
		if (deflectFromWalls) {
			controlValue = 0.1f;
		}
		else {
			controlValue = 0.0f;
		}

		if (p > controlValue) {//moving along the route
			hasMoved = moveAgent(perpendicularAgents, i, up);
		}
		else {//wall deflection
			if (perpendicularAgents[i].x < getSizeX() / 2) {
				hasMoved = moveAgent(perpendicularAgents, i, right);
			}
			else {
				hasMoved = moveAgent(perpendicularAgents, i, left);
			}
		}

		if (!hasMoved) {
			moveAgentRandomly(perpendicularAgents, i, left, right, up);
		}
	}

	//gathering data
	if (perpendicularAgents.size() > 0) {
		if (getCurrentEndFlowRate() > 0 && getCurrentPEndFlowRate() > 0) {
			reachedEnd = true;
		}
	} else {
		if (getCurrentEndFlowRate() > 0) {
			reachedEnd = true;
		}
	}

	if (perpendicularAgents.size() > 0) {
		if (reachedEnd == true) {
			startFlowRate.push_back(getCurrentStartFlowRate());
			endFlowRate.push_back(getCurrentEndFlowRate());

			startPFlowRate.push_back(getCurrentPStartFlowRate());
			endPFlowRate.push_back(getCurrentPEndFlowRate());
		}
	}
	else {
		if (reachedEnd == true) {
			startFlowRate.push_back(getCurrentStartFlowRate());
			endFlowRate.push_back(getCurrentEndFlowRate());
		}
	}
}

bool AgentsGrid::isSimulationOver() {
	return simulationOver;
}

color AgentsGrid::getAgentColor(int agentIndex) {
	if (agentIndex < 0 || agentIndex >= agents.size()) {
		//printf("Wrong agent index!\n");
	} else {
		return agents[agentIndex].color;
	}
}

int AgentsGrid::getAgentX(int agentIndex) {
	if (agentIndex < 0 || agentIndex >= agents.size()) {
		//printf("Wrong agent index!\n");
	}
	else {
		return agents[agentIndex].x;
	}
}

int AgentsGrid::getAgentY(int agentIndex) {
	if (agentIndex < 0 || agentIndex >= agents.size()) {
		//printf("Wrong agent index!\n");
	}
	else {
		return agents[agentIndex].y;
	}
}

unsigned int AgentsGrid::getAgentsNumber() {
	return agents.size();
}

color AgentsGrid::getPAgentColor(int agentIndex) {
	if (agentIndex < 0 || agentIndex >= perpendicularAgents.size()) {
		//printf("Wrong agent index!\n");
	}
	else {
		return perpendicularAgents[agentIndex].color;
	}
}

int AgentsGrid::getPAgentX(int agentIndex) {
	if (agentIndex < 0 || agentIndex >= perpendicularAgents.size()) {
		//printf("Wrong agent index!\n");
	}
	else {
		return perpendicularAgents[agentIndex].x;
	}
}

int AgentsGrid::getPAgentY(int agentIndex) {
	if (agentIndex < 0 || agentIndex >= perpendicularAgents.size()) {
		//printf("Wrong agent index!\n");
	}
	else {
		return perpendicularAgents[agentIndex].y;
	}
}

unsigned int AgentsGrid::getPAgentsNumber() {
	return perpendicularAgents.size();
}

//works only for horizontal corridors
void AgentsGrid::createNarrowing() {
	int narrowingsNumber = size_y / 2 - 3;
	int narrowingMultiplier = (size_x-1) / narrowingsNumber;

	for (int j = 0; j < narrowingsNumber; j++) {
		for (int i = j*narrowingMultiplier; i < size_x; i++) {
			setCellAccessibility(i, j, false);
			setCellAccessibility(i, size_y-j-1, false);
		}
	}
}

//works only for horizontal corridors
void AgentsGrid::createRandomNarrowing() {
	for (int i = (int)(0.3 * size_x); i < (int)(0.8*size_x); i++) {
		for (int j = 0; j < 6; j++) {
			setCellAccessibility(i, j, false);
			setCellAccessibility(i, size_y - j, false);
		}
	}

	for (int i = (int)(0.6 * size_x); i < (int)(0.8*size_x); i++) {
		for (int j = 6; j < 12; j++) {
			setCellAccessibility(i, j, false);
		}
	}

	for (int i = (int)(0.35 * size_x); i < (int)(0.55*size_x); i++) {
		for (int j = 6; j < 12; j++) {
			setCellAccessibility(i, size_y - j, false);
		}
	}
}

void AgentsGrid::createParameterizedNarrowing(unsigned int a) {
	if (a < (int)(size_y / 4)) {
		for (int i = (int)(0.35 * size_x); i < (int)(0.65*size_x); i++) {
			for (int j = 0; j < a; j++) {
				setCellAccessibility(i, j, false);
				setCellAccessibility(i, size_y - j - 1, false);
			}
		}

		for (int i = (int)(0.45 * size_x); i < (int)(0.55*size_x); i++) {
			for (int j = a; j < 2*a; j++) {
				setCellAccessibility(i, j, false);
				setCellAccessibility(i, size_y - j - 1, false);
			}
		}
	}
	else {
		printf("Parameter 'a' is to high!\n");
	}
}

void AgentsGrid::createDoor(unsigned int a) {
	if (a < (int)(size_y / 2)) {
		for (int i = 0; i < a; i++) {
			setCellAccessibility((int)(0.5*size_x), i, false);
			setCellAccessibility((int)(0.5*size_x), size_y - i - 1, false);
		}
	}
	else {
		printf("Parameter 'a' is to high!\n");
	}	
}

//methods for horizontal corridor
unsigned int AgentsGrid::getCurrentStartFlowRate() {
	unsigned int FlowRate = 0;

	for (int j = 0; j < size_y; j++) {
		if (getCellOccupation((int)(0.3*size_x), j) == false) {
			FlowRate++;
		}
	}

	return FlowRate;
}

unsigned int AgentsGrid::getCurrentEndFlowRate() {
	unsigned int FlowRate = 0;

	for (int j = 0; j < size_y; j++) {
		if (getCellOccupation((int)(0.8*size_x), j) == false) {
			FlowRate++;
		}
	}

	return FlowRate;
}

unsigned int AgentsGrid::getCurrentPStartFlowRate() {
	unsigned int FlowRate = 0;

	for (int j = 0; j < size_x; j++) {
		if (getCellOccupation(j, (int)(0.3*size_y)) == false) {
			FlowRate++;
		}
	}

	return FlowRate;
}

unsigned int AgentsGrid::getCurrentPEndFlowRate() {
	unsigned int FlowRate = 0;

	for (int j = 0; j < size_x; j++) {
		if (getCellOccupation(j, (int)(0.8*size_y)) == false) {
			FlowRate++;
		}
	}

	return FlowRate;
}

float AgentsGrid::getAverageFlowRate(std::vector <unsigned int> flowRate) {
	float FlowRate = 0.0f;

	for (int i = 0; i < flowRate.size(); i++) {
		FlowRate += (float)flowRate[i];
	}

	return FlowRate/(float)flowRate.size();
}

float AgentsGrid::getAverageFlowRateStart() {
	return getAverageFlowRate(startFlowRate);
}

float AgentsGrid::getAverageFlowRateEnd() {
	return getAverageFlowRate(endFlowRate);
}

void AgentsGrid::saveFlowRatesToFile(float agentsDensity, float perpendicularAgentsDensity, std::string filename, bool intersection) {
	//remove(filename.c_str());
	FILE *file = fopen(filename.c_str(), "a");

	if (intersection) {
		fprintf(file, "%f %f %f %f %f %f %f %f %f %f\n", agentsDensity, perpendicularAgentsDensity, 
			getAverageFlowRate(startFlowRate), getAverageFlowRate(endFlowRate),
			getAverageFlowRate(startPFlowRate), getAverageFlowRate(endPFlowRate));
	}
	else {
		fprintf(file, "%f %f %f\n", agentsDensity, getAverageFlowRate(startFlowRate), getAverageFlowRate(endFlowRate));
	}
	

	fclose(file);
}