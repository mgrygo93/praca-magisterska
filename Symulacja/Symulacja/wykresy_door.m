data1 = load('data_a=0.txt');
data2 = load('data_a=2.txt');
data3 = load('data_a=4.txt');
data4 = load('data_a=6.txt');
data5 = load('data_a=8.txt');
data6 = load('data_a=10.txt');
data7 = load('data_a=11.txt');
data8 = load('data_a=12.txt');
data9 = load('data_a=13.txt');

density1 = data1(:,1); density2 = data2(:,1); density3 = data3(:,1); density4 = data4(:,1); density5 = data5(:,1);
density6 = data6(:,1); density7 = data7(:,1); density8 = data8(:,1); density9 = data9(:,1);

start_point1 = data1(:,3); start_point2 = data2(:,3); start_point3 = data3(:,3); start_point4 = data4(:,3); start_point5 = data5(:,3);
start_point6 = data6(:,3); start_point7 = data7(:,3); start_point8 = data8(:,3); start_point9 = data9(:,3);

end_point1 = data1(:,4); end_point2 = data2(:,4); end_point3 = data3(:,4); end_point4 = data4(:,4); end_point5 = data5(:,4);
end_point6 = data6(:,4); end_point7 = data7(:,4); end_point8 = data8(:,4); end_point9 = data9(:,4);

average1(:,1) = start_point1; average1(:,2) = end_point1;
average2(:,1) = start_point2; average2(:,2) = end_point2;
average3(:,1) = start_point3; average3(:,2) = end_point3;
average4(:,1) = start_point4; average4(:,2) = end_point4;
average5(:,1) = start_point5; average5(:,2) = end_point5;
average6(:,1) = start_point6; average6(:,2) = end_point6;
average7(:,1) = start_point7; average7(:,2) = end_point7;
average8(:,1) = start_point8; average8(:,2) = end_point8;
average9(:,1) = start_point9; average9(:,2) = end_point9;

[~,idx] = sort(average1(:,1)); % sort just the first column
sortedmat1 = average1(idx,:);   % sort the whole matrix using the sort indices

[~,idx] = sort(average2(:,1));
sortedmat2 = average2(idx,:);

[~,idx] = sort(average4(:,1));
sortedmat4 = average4(idx,:);

[~,idx] = sort(average5(:,1));
sortedmat5 = average5(idx,:);

[~,idx] = sort(average6(:,1));
sortedmat6 = average6(idx,:);

[~,idx] = sort(average7(:,1));
sortedmat7 = average7(idx,:);

[~,idx] = sort(average8(:,1));
sortedmat8 = average8(idx,:);

[~,idx] = sort(average9(:,1));
sortedmat9 = average9(idx,:);

#sr. agenci (gestosc)
figure(1)
plot(density1, end_point1, 'color', [0.9, 0.0, 0.0],
  density2, end_point2, 'color', [0.6, 0.0, 0.0],
  density3, end_point3, 'color', [0.3, 0.0, 0.0],
  density4, end_point4, 'color', [0.0, 0.3, 0.0],
  density5, end_point5, 'color', [0.0, 0.6, 0.0],
  density6, end_point6, 'color', [0.0, 0.9, 0.0],
  density7, end_point7, 'color', [0.0, 0.0, 0.3],
  density8, end_point8, 'color', [0.0, 0.0, 0.6],
  density9, end_point9, 'color', [0.0, 0.0, 0.9]);
set (0, "defaultlinelinewidth", 2) 
title('Wyjscie (wejscie) - drzwi'); legend("A=0", "A=2", "A=4", "A=6", "A=8", "A=10", "A=11", "A=12", "A=13", "Location", "NorthWest");
xlabel('Gestosc agentow (wejscie)'); ylabel('Sr. liczba agentow (wyjscie)');
xlim([0 0.61]); ylim([0 12.5]);

#sr. agenci (sr. agenci)
figure(2)
plot(sortedmat1(:,1), sortedmat1(:,2), 'color', [0.9, 0.0, 0.0],
  sortedmat2(:,1), sortedmat2(:,2), 'color', [0.6, 0.0, 0.0],
  sortedmat3(:,1), sortedmat3(:,2), 'color', [0.3, 0.0, 0.0],
  sortedmat4(:,1), sortedmat4(:,2), 'color', [0.0, 0.3, 0.0],
  sortedmat5(:,1), sortedmat5(:,2), 'color', [0.0, 0.6, 0.0],
  sortedmat6(:,1), sortedmat6(:,2), 'color', [0.0, 0.9, 0.0],
  sortedmat7(:,1), sortedmat7(:,2), 'color', [0.0, 0.0, 0.3],
  sortedmat8(:,1), sortedmat8(:,2), 'color', [0.0, 0.0, 0.6],
  sortedmat9(:,1), sortedmat9(:,2), 'color', [0.0, 0.0, 0.9]);
set (0, "defaultlinelinewidth", 1.5) 
title('Wyjscie (wejscie) - drzwi'); legend("A=0", "A=2", "A=4", "A=6", "A=8", "A=10", "A=11", "A=12", "A=13", "Location", "NorthWest");
xlabel('Sr. liczba agentow (wejscie)'); ylabel('Sr. liczba agentow (wyjscie)');
xlim([0 16]); ylim([0 12.5]);