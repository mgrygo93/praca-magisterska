#pragma once

void Visualization(AgentsGrid &agentsGrid, bool slowDown, float agentDensity, float perpendicularAgentDensity,
	std::array <unsigned int, 4> startArea1);
