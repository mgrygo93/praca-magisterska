#include <stdio.h>
#include "Grid.h"

Grid::Grid() {
}

Grid::Grid(unsigned int sizeX, unsigned int sizeY) {
	size_x = sizeX;
	size_y = sizeY;
}

Grid::~Grid() {
}

void Grid::writeGrid() {
	for (unsigned int i = 0; i < size_y; i++) {
		for (unsigned int j = 0; j < size_x; j++) {
			if (cell_grid[i][j].is_available == false) {
				printf("X");
			} else {
				if (cell_grid[i][j].is_free == true) {
					printf("1");
				} else {
					printf("0");
				}
			}
			printf(" ");
		}
		printf("\n");
	}
}

//we start indexing from 0
void Grid::denyCellAccess(int x, int y) {
	if (x >= size_x || y >= size_y || x < 0 || y < 0) {
		//printf("Cell out of bounds!\n");
	} else {
		cell_grid[y][x].is_available = false;
	}
}

unsigned int Grid::getSizeX() {
	return size_x;
}

unsigned int Grid::getSizeY() {
	return size_y;
}

bool Grid::getCellAccessibility(int x, int y) {
	if (x >= size_x || y >= size_y || x < 0 || y < 0) {
		//printf("Cell out of bounds!\n");
		return 0;
	} else {
		return cell_grid[y][x].is_available;
	}
}

void Grid::setCellAccessibility(int x, int y, bool isAccessible) {
	if (x >= size_x || y >= size_y || x < 0 || y < 0) {
		//printf("Cell out of bounds!\n");
	}
	else {
		cell_grid[y][x].is_available = isAccessible;
	}
}

bool Grid::getCellOccupation(int x, int y) {
	if (x >= size_x || y >= size_y || x < 0 || y < 0) {
		//printf("Cell out of bounds!\n");
		return 0;
	}
	else {
		return cell_grid[y][x].is_free;
	}
}

void Grid::setCellOccupation(int x, int y, bool isFree) {
	if (x >= size_x || y >= size_y || x < 0 || y < 0) {
		//printf("Cell out of bounds!\n");
	}
	else {
		cell_grid[y][x].is_free = isFree;
	}
}