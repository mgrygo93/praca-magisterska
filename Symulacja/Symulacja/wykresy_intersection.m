data = load('data_intersection.txt');

%subplot(2,2,1);
figure(1);
scatter(data(:,3),data(:,7),64,data(:,4),'filled');
h = colorbar; caxis([0,11]); colormap(jet); ylabel(h,'Sr. liczba agentow (wyjscie A*)')
xlabel('Sr. liczba agentow (wejscie A)'); ylabel('Sr. liczba agentow (wejscie B)');
title('Wyjscie A* (wejscie A, wejscie B)');

%subplot(2,2,2);
figure(2);
scatter(data(:,1),data(:,2),64,data(:,4),'filled'); xlim([0,0.61]); ylim([0,0.61]);
h = colorbar; caxis([0,11]); colormap(jet); ylabel(h,'Sr. liczba agentow (wyjscie A*)')
xlabel('Gestosc agentow (wejscie A)'); ylabel('Gestosc agentow (wejscie B)');
title('Wyjscie A* (gestosc A, gestosc B)');

%subplot(2,2,3);
figure(3);
scatter(data(:,3),data(:,7),64,data(:,8),'filled');
h = colorbar; caxis([0,11]); colormap(jet); ylabel(h,'Sr. liczba agentow (wyjscie B*)')
xlabel('Sr. liczba agentow (wejscie A)'); ylabel('Sr. liczba agentow (wejscie B)');
title('Wyjscie B* (wejscie A, wejscie B)');

%subplot(2,2,4);
figure(4);
scatter(data(:,1),data(:,2),64,data(:,8),'filled'); xlim([0,0.61]); ylim([0,0.61]);
h = colorbar; caxis([0,11]); colormap(jet); ylabel(h,'Sr. liczba agentow (wyjscie B*)')
xlabel('Gestosc agentow (wejscie A)'); ylabel('Gestosc agentow (wejscie B)');
title('Wyjscie B* (gestosc A, gestosc B)');
