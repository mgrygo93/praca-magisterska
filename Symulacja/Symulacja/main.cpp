#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>  
#include <string>
#include <array>
#include "AgentsGrid.h"
#include "graphical_functions.h"

using namespace std;

const unsigned int spawn_rate = 30;

void write_estimated_time_left(int current_timestep_number, int number_of_iterations, float operations_time_so_far) {
	if (current_timestep_number == 0) {
		printf("\r                                                                 ");
		printf("\r%d/%d iteration in progress!", current_timestep_number + 1, number_of_iterations);
	}
	else {
		float estimatedTimeLeft = operations_time_so_far / current_timestep_number * (number_of_iterations - current_timestep_number);
		if (estimatedTimeLeft < 60) {
			printf("\r                                                                 ");
			printf("\r%d/%d iteration! Time to finish: %f[s]", current_timestep_number + 1, number_of_iterations, estimatedTimeLeft);
		}
		else if (estimatedTimeLeft > 60 && estimatedTimeLeft < 3600) {
			double seconds, minutes;
			seconds = modf(estimatedTimeLeft / 60, &minutes);
			seconds *= 60;
			printf("\r                                                                 ");
			printf("\r%d/%d iteration! Time to finish: %d[min] %d[s]", current_timestep_number + 1, number_of_iterations, (int)minutes, (int)seconds);
		}
		else {
			double hours, seconds, minutes;
			seconds = modf(estimatedTimeLeft / 60, &minutes);
			seconds *= 60;
			minutes = modf(minutes / 60, &hours);
			minutes *= 60;
			printf("\r                                                                 ");
			printf("\r%d/%d iteration! Time to finish: %d[h] %d[min] %d[s]", current_timestep_number + 1, number_of_iterations, (int)hours, (int)minutes, (int)seconds);
		}
	}
}

void Simulation(AgentsGrid &agentsGrid, float agentsDensity, int iterationsNumber) {
	int size_y = agentsGrid.getSizeY();

	for (int j = 0; j < iterationsNumber; j++) {
		if (agentsGrid.isSimulationOver() == false) {
			if (agentsGrid.checkStartArea(0, 0, 8, size_y)) {
				agentsGrid.setAgentsRandomly(0, 0, 6, size_y, 1, 0.0f, 0.0f, 1.0f, agentsDensity, false);
			}
			agentsGrid.singleIteration();
		}
	}	
}

void SimulationIntersection(AgentsGrid &agentsGrid, float agentsDensity, float perpendicularAgentsDensity, int iterationsNumber) {
	const unsigned int tmp = (136 - 28) / 2;

	for (int j = 0; j < iterationsNumber; j++) {
		if (agentsGrid.isSimulationOver() == false) {
			if (agentsGrid.checkStartArea(0, tmp, 6, 28)) {
				agentsGrid.setAgentsRandomly(0, tmp, 6, 28, 1, 0.0f, 0.0f, 1.0f, agentsDensity, false);
				
			}
			if (agentsGrid.checkStartArea(tmp, 0, 28, 6)) {
				agentsGrid.setAgentsRandomly(tmp, 0, 28, 6, 1, 1.0f, 0.0f, 0.0f, perpendicularAgentsDensity, true);
			}
			agentsGrid.singleIteration();
		}
	}
}

void SimulationSeries(unsigned int a, bool door, bool gradual) {
	float agentsDensity = 0.01f;
	float operations_time_so_far = 0.0f;
	clock_t t1, t2;
	const int iterationsNumber = 60;
	std::string filename;

	if (door == true && gradual == true) {
		printf("There can't both types of narrowings at the same time!");
	}
	else {
		for (int i = 0; i < iterationsNumber; i++) {
			t1 = clock();
			write_estimated_time_left(i, iterationsNumber, operations_time_so_far);
			AgentsGrid grid = AgentsGrid(136, 28, true, false);

			if (door == true) {
				grid.createDoor(a);
				filename = "data_A=" + std::to_string(a) + ".txt";
			}

			if (gradual == true) {
				grid.createParameterizedNarrowing(a);
				filename = "data_B=" + std::to_string(a) + ".txt";
			}

			Simulation(grid, agentsDensity, 500);

			grid.saveFlowRatesToFile(agentsDensity, 0.0, filename, false);

			agentsDensity += 0.01f;

			t2 = clock();
			operations_time_so_far += (float)(t2 - t1) / 1000;
		}
	}	
}

//series of simulations to determine critical point of blockade
std::array <float, 2> SimulationCriticalPoint(unsigned int a, bool door, bool gradual) {
	std::array <float, 2> criticalPoint;	

	std::vector <float> critPoint, critPointDens, flowRateEnd;	

	float agentsDensity = 0.01f;
	const int iterationsNumber = 60;
	unsigned int iterator = iterationsNumber - 10;

	if ((door == true && gradual == true) || (door == false && gradual == false)) {
		printf("There can't both types of narrowings or neither!");
	}
	else {
		for (int i = 0; i < iterationsNumber; i++) {
			AgentsGrid grid = AgentsGrid(136, 28, true, false);

			if (door == true) {
				grid.createDoor(a);
			}

			if (gradual == true) {
				grid.createParameterizedNarrowing(a);
			}

			Simulation(grid, agentsDensity, 500);
			
			//detecting critical point
			flowRateEnd.push_back(grid.getAverageFlowRateEnd());
			critPoint.push_back(grid.getAverageFlowRateStart());
			critPointDens.push_back(agentsDensity);

			if (i > 9) {
				if (fabs(flowRateEnd[i] - flowRateEnd[i-9]) < 0.25f) {
					iterator = i-9;
					break;
				}
			}
			//end of detecting critical point

			agentsDensity += 0.01f;
		}
	}

	criticalPoint[0] = critPointDens[iterator];
	criticalPoint[1] = critPoint[iterator];

	return criticalPoint;
}

std::array <float, 4> SimulationCriticalPointSeries(unsigned int a, bool door, bool gradual) {
	std::array <float, 4> criticalPointsSeries;

	if ((door == true && gradual == true) || (door == false && gradual == false)) {
		printf("There can't both types of narrowings or neither!");
	}
	else {
		std::vector <std::array <float, 2>> criticalPoints;
		const int iterations_number = 100;

		for (int i = 0; i < iterations_number; i++) {
			printf("\r                                                                 ");
			printf("\rIteration number: %d/%d", i + 1, iterations_number);
			criticalPoints.push_back(SimulationCriticalPoint(a, door, gradual));
		}

		float AVG_CritPointDens = 0.0f, AVG_CritPoint = 0.0f;
		float STDEV_CritPointDens = 0.0f, STDEV_CritPoint = 0.0f;

		for (size_t i = 0; i < criticalPoints.size(); i++) {
			AVG_CritPointDens += criticalPoints[i][0];
			AVG_CritPoint += criticalPoints[i][1];
		}
		AVG_CritPointDens /= criticalPoints.size();
		AVG_CritPoint /= criticalPoints.size();

		for (size_t i = 0; i < criticalPoints.size(); i++) {
			STDEV_CritPointDens += (criticalPoints[i][0] - AVG_CritPointDens)*(criticalPoints[i][0] - AVG_CritPointDens);
			STDEV_CritPoint += (criticalPoints[i][1] - AVG_CritPoint)*(criticalPoints[i][1] - AVG_CritPoint);
		}
		STDEV_CritPointDens = sqrt(STDEV_CritPointDens / criticalPoints.size());
		STDEV_CritPoint = sqrt(STDEV_CritPoint / criticalPoints.size());

		criticalPointsSeries[0] = AVG_CritPointDens;
		criticalPointsSeries[1] = STDEV_CritPointDens;
		criticalPointsSeries[2] = AVG_CritPoint;
		criticalPointsSeries[3] = STDEV_CritPoint;
	}	
	
	return criticalPointsSeries;
}

void SimulationSeriesIntersection(float maxDensity) {
	float agentsDensity = 0.01f, pAgentsDensity = 0.01f;
	float operations_time_so_far = 0.0f;
	clock_t t1, t2;
	const int iterationsNumber = (int)(maxDensity/0.01f);
	std::string filename = "data_intersection.txt";

	for (int i = 0; i < iterationsNumber; i++) {
		for (int j = 0; j < iterationsNumber; j++) {
			t1 = clock();
			write_estimated_time_left(j+(iterationsNumber)*i, iterationsNumber*iterationsNumber, operations_time_so_far);

			AgentsGrid grid = AgentsGrid(136, 28, true, true);

			SimulationIntersection(grid, agentsDensity, pAgentsDensity, 500);
			grid.saveFlowRatesToFile(agentsDensity, pAgentsDensity, filename, true);

			agentsDensity += 0.01f;		

			t2 = clock();
			operations_time_so_far += (float)(t2 - t1) / 1000;
		}
		agentsDensity = 0.01f;
		pAgentsDensity += 0.01f;		
	}
	
}

int main() {
	srand(time(NULL));

	int check = 0, narrowing = 0, architecture = 0;
	float agentsDensity = 0.0f, perpendicularAgentsDensity = 0.0f;
	cout << "Please choose your option:\n";
	cout << "1 - Simulation\n";
	cout << "2 - Visualization\n";
	cout << "3 - Critical point simulation\n";
	cin >> check;

	switch (check) {
	case 1:	{
		cout << "What type of architecture?\n";
		cout << "1 - Door type narrowing\n";
		cout << "2 - Gradual narrowing\n";
		cout << "3 - Intersection\n";
		cin >> architecture;

		switch(architecture) {
		case 1: {
			const int datapoints[9] = { 0,2,4,6,8,10,11,12,13 };
			for (int i = 0; i < 9; i++) {
				cout << "A = " << datapoints[i] << ", " << i + 1 << "/" << 9 << endl;
				SimulationSeries(datapoints[i], true, false);
				printf("\r                                                                 ");
				printf("\rDone!");
				cout << endl;
			}
			break;
		}
		case 2: {
			const int maxValue = 7;
			for (int i = 0; i < maxValue; i++) {
				cout << "B = " << i << ", " << i + 1 << "/" << maxValue << endl;
				SimulationSeries(i, false, true);
				printf("\r                                                                 ");
				printf("\rDone!");
				cout << endl;
			}
			break;
		}
		case 3: {
			SimulationSeriesIntersection(0.6f);
			break;
		}
		default: {
			printf("Wrong value given!");
			break;
		}
		}
				
		break;
	}
	case 2:	{
		cout << "What type of narrowing?\n";
		cout << "1 - Door\n";
		cout << "2 - Gradual narrowing\n";
		cout << "3 - Intersection\n";
		cin >> architecture;

		if (architecture != 3) {
			if(architecture == 1) {
				cout << "How big is corridor's narrowing? Integer value from <0,13>\n";
			} else {
				cout << "How big is corridor's narrowing? Integer value from <0,6>\n";
			}
			cin >> narrowing;

			if (architecture == 1) {
				if (narrowing < 0 || narrowing > 13) {
					cout << "Wrong value given!" << endl;
					break;
				}
			}
			else {
				if (narrowing < 0 || narrowing > 6) {
					cout << "Wrong value given!" << endl;
					break;
				}
			}
		}		
		
		cout << "What's agents' density at the spawn point? (0.0 - 1.0)\n";
		cin >> agentsDensity;

		if (architecture == 3) {
			cout << "What's perpendicular agents' density at the spawn point? (0.0 - 1.0)\n";
			cin >> perpendicularAgentsDensity;
		}

		std::array <unsigned int, 4> startArea = { 0,0,6,28 };

		switch (architecture) {
		case 1: {
			AgentsGrid grid = AgentsGrid(136, 28, true, false);
			grid.createDoor(narrowing);
			Visualization(grid, false, agentsDensity, 0.00f, startArea);
			break;
		}
		case 2: {
			AgentsGrid grid = AgentsGrid(136, 28, true, false);
			grid.createParameterizedNarrowing(narrowing);
			Visualization(grid, false, agentsDensity, 0.00f, startArea);
			break;
		}
		case 3: {
			AgentsGrid grid = AgentsGrid(136, 28, true, true);
			const unsigned int tmp = (136 - 28) / 2;
			std::array <unsigned int, 4> startArea = { 0,tmp,6,28 };
			Visualization(grid, false, agentsDensity, perpendicularAgentsDensity, startArea);
			break;
		}
		default: {
			printf("Wrong value given!");
			break; 
		}
		}

		break;
	}	
	case 3: {
		const std::array <unsigned int, 6> a = { 6,8,10,11,12,13 };
		const std::array <unsigned int, 4> b = { 3,4,5,6 };
				
		cout << "DOOR SIMULATIONS" << endl;

		for (int i = 0; i < a.size(); i++) {
			cout << "\na = " << a[i] << endl;
			std::array <float, 4> crit;
			crit = SimulationCriticalPointSeries(a[i], true, false);

			FILE *file1 = fopen("critical_points_DOOR.txt", "a");
			fprintf(file1, "%u %f %f %f %f\n", a[i], crit[0], crit[1], crit[2], crit[3]);
			fclose(file1);
		}
		
		cout << "\nGRADUAL NARROWING SIMULATIONS" << endl;

		for (int i = 0; i < b.size(); i++) {
			cout << "\nb = " << b[i] << endl;
			std::array <float, 4> crit;
			crit = SimulationCriticalPointSeries(b[i], false, true);

			FILE *file2 = fopen("critical_points_GRADUAL.txt", "a");
			fprintf(file2, "%u %f %f %f %f\n", b[i], crit[0], crit[1], crit[2], crit[3]);
			fclose(file2);
		}
		break;
	}
	default:
		break;
	}
	return 0;
}